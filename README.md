# Linux Komut

```
$whoami

$wall 'hi all'


echo "Lets join us, MOO "|cowsay
 ____________________
< Lets join us, MOO  >
 --------------------
        \   ^__^
         \  (oo)\_______
            (__)\       )\/\
                ||----w |
                ||     ||


```
```
history | awk '{a[$2]++}END{for(i in a){print a[i] " " i}}' | sort -rn | head
en cok kullanılandan en aza sıralama

netstat -an | grep ESTABLISHED | awk '{print $5}' | awk -F: '{print $1}' | sort | uniq -c | awk '{ printf("%s\t%s\t",$2,$1) ; for (i = 0; i < $1; i++) {printf("*")}; print "" }'
total baglantı sayisi

du --max-depth=1 | sort -r -n | awk '{split("k m g",v); s=1; while($1>1024){$1/=1024; s++} print int($1)" "v[s]"\t"$2}'
boyuta göre 

while [ /bin/true ]; do OLD=$NEW; NEW=`cat /proc/net/dev | grep eth0 | tr -s ' ' | cut -d' ' -f "3 11"`; echo $NEW $OLD | awk '{printf("\rin: % 9.2g\t\tout: % 9.2g", ($1-$3)/1024, ($2-$4)/1024)}'; sleep 1; done
gelen trafik;


seq 50| awk 'BEGIN {a=1; b=1} {print a; c=a+b; a=b; b=c}'
fibonacci;


t=$(df|awk 'NR!=1{sum+=$2}END{print sum}');sudo du / --max-depth=1|sed '$d'|sort -rn -k1 | awk -v t=$t 'OFMT="%d" {M=64; for (a=0;a<$1;a++){if (a>c){c=a}}br=a/c;b=M*br;for(x=0;x<b;x++){printf "\033[1;31m" "|" "\033[0m"}print " "$2" "(a/t*100)"% total"}'
boyut grafik tipinde;


find . -type f | awk -F'.' '{print $NF}' | sort| uniq -c | sort -g
dosya türlerinin listesi & dosya sayısı
```

komut satırında ya da spyder'da ! ile başlanarak son çalıştırılışmış program exit alınabilir.
windows'da bunun için
echo %errorlevel%

linux:
echo$?

****************************************************************
#Command Linux
****************************************************************


echo “this is the test command” | pv -qL 5


_**SİSTEM BİLGİSİ KOMUTLARI**_

```
uname -a # Linux sistem bilgilerini görüntüler
uname -r # Çekirdek sürüm bilgilerini görüntüler
uptime # Sistemin ne kadar süredir çalıştığını gösterir
hostname # Ana bilgisayar adını gösterir
hostname -I # Ana bilgisayarın IP adreslerini görüntüler
last reboot # Sistem yeniden başlatma geçmişini gösterir
date # Geçerli tarih ve saati gösterir
cal # Bu ayın takvimini gösterir
w # Kimin çevrimiçi olduğunu gösterir
```

_**DONANIM BİLGİSİ KOMUTLARI**_

```
dmesg # Çekirdek arabelleğindeki mesajları görüntüler
cat /proc/cpuinfo # CPU bilgilerini görüntüler
cat /proc/meminfo # Bellek bilgilerini görüntüler
free -h # Toplam, boş ve kullanılan belleği görüntüler
lspci -tv # PCI aygıtları gösterir
lsusb -tv # USB aygıtları gösterir
dmidecode # BIOS'tan DMI/SMBIOS bilgisini görüntüler
hdparm -i /dev/sda # sda disk hakkında bilgileri gösterir
hdparm -tT /dev/sda # sda disk üzerinde bir okuma hızı testi yapar
badblocks -s /dev/sda # sda diskte okunamayan blokları test eder
```

_**PERFORMANS İZLEME VE İSTATİSTİK KOMUTLARI**_

```
top # En önemli süreçleri görüntüler ve yönetir
htop # Etkileşimli süreç görüntüleyici
mpstat 1 # İşlemci ile ilgili istatistikleri görüntüler
vmstat 1 # Sanal bellek istatistiklerini görüntüler
iostat 1 # I/O istatistiklerini görüntüler
tcpdump -i eth0 # eth0 arabirimindeki tüm paketleri yakalar ve görüntüler
lsof # Sistemdeki tüm açık dosyaları listeler
lsof -u user # Kullanıcı tarafından açılan dosyaları listeler
watch df -h # Periyodik güncellemeleri gösteren "df -h" komutunu yürütür
lsblk #disk show tree

```

_**KULLANICI BİLGİLERİ VE YÖNETİMİ KOMUTLARI**_

```
id # Mevcut kullanıcınızın kullanıcı ve grup kimliklerini görüntüler
last # Sisteme en son giriş yapan kullanıcıları görüntüler
who # Sisteme kimin giriş yaptığını gösterir
groupadd test # "test" adında bir grup oluşturur
useradd -c "Cemal Taner" -m cemal # "Cemal Taner" açıklamasıyla
cemal adında bir hesap ve kullanıcının ana dizinini oluşturur
userdel cemal # cemal hesabını siler
usermod -aG test cemal # cemal hesabını test grubuna ekler
```
_**DOSYA VE DİZİN KOMUTLARI**_

```
ls -al # Tüm dosyaları ayrıntılı bir liste formatında gösterir
pwd # Mevcut çalışma dizinini gösterir
mkdir directory # Bir dizin oluşturur
rm file # Dosyayı kaldırır (siler)
rm -r directory #Dizini ve içeriğini yinelemeli olarak kaldırır
rm -f file # Onay istemeden dosyanın kaldırılmasını zorlar
rm -rf directory # Dizini yinelemeli olarak zorla kaldırır
cp dosya1 dosya2 # Dosya1'i dosya2'ye kopyalar
cp -r source_directory destination # Kaynak_dizini yinelemeli olarak hedefe kopyalar. Hedef varsa, kaynak_dizini hedefe kopyalar, aksi takdirde kaynak_dizinin içeriğiyle hedefi oluşturur.
mv dosya1 dosya2 # Dosya1'i dosya2'ye yeniden adlandırır veya taşır. Dosya2 mevcut bir dizinse, dosya1'i dosya2 dizinine taşır
ln -s /path/to/file linkname # Linkname için sembolik link oluşturur
touch file # Boş bir dosya oluşturur veya dosyanın erişim ve değişiklik zamanlarını günceller.
cat file # Dosyanın içeriğini görüntüler
less file # Bir metin dosyasına göz atar /more
head file # Dosyanın ilk 10 satırını gösterir
tail file # Dosyanın son 10 satırını gösterir
tail -f file # Dosyanın son 10 satırını görüntüler ve dosyayı takip eder
```

![dosya dizin](dosyadizin.png)


_**SÜREÇ YÖNETİMİ KOMUTLARI**_

```

grep -A10 -B10 "exception" *.txt
ps # Şu anda çalışan işlemleri görüntüler
ps -ef # Sistemde şu anda çalışan tüm işlemleri görüntüler
ps -ef | grep processname #processname için işlem bilgilerini görüntüler

top 
---> Big M
kill -9 <pid>

ps -auxf | sort -nr -k 3 | head -10 ; list of process that high performance for memory
ps -auxf | sort -nr -k 4 | head -5; lifst of process that high perfomnce for cpu

top -o %MEM

ps -eo pid,ppid,cmd,comm,%mem,%cpu --sort=-%mem | head -10

  PID  PPID CMD                         COMMAND         %MEM %CPU
 2806     1 /opt/google/chrome/chrome - chrome           4.5  4.3
 8164  2828 /opt/google/chrome/chrome - chrome           3.2  6.2
 8115  2828 /opt/google/chrome/chrome - chrome           3.0  7.8
 2314  2176 /usr/bin/gnome-shell        gnome-shell      2.9  4.4

ps aux --sort -%mem | head -10

To clear the buffers and cache, run the following command:

# echo 3 > /proc/sys/vm/drop_caches

The command execution may take several seconds, depending on the memory size. After the command is executed, the occupied memory will be released.

top -o %CPU | head -n 10


ps -aux |grep -v grep |grep -i elasticsearch |awk ‘{print$2}’ | xargs kill -9
pgrep httpd
pstree -ap <pid> , ps -axf
ps -eo pid ; all process pid


ps -eocomm,pcpu | egrep -v '(0.0)|(%CPU)'

ps -eocomm,pmem | egrep -v '(0.0)|(%MEM)'


#!/bin/bash
#Name: monitor_cpu_usage.sh
#Description: Script to check top cpu consuming process for 1 hour
#Change the SECS to total seconds to monitor CPU usage.
#UNIT_TIME is the interval in seconds between each sampling
function report_utilisation {
  # Process collected data
  echo
  echo "CPU eaters :"
cat /tmp/cpu_usage.$$ | 
awk '
{ process[$1]+=$2; }
END{
  for(i in process)
  {
    printf("%-20s %s\n",i, process[i]) ;
  }
}' | sort -nrk 2 | head
#Remove the temporary log file
rm /tmp/cpu_usage.$$
exit 0
}
trap 'report_utilisation' INT
SECS=3600
UNIT_TIME=10
STEPS=$(( $SECS / $UNIT_TIME ))
echo "Watching CPU usage... ;"
# Collect data in temp file
for((i=0;i<$STEPS;i++)); do
    ps -eocomm,pcpu | egrep -v '(0.0)|(%CPU)' >> /tmp/cpu_usage.$$
    sleep $UNIT_TIME
done
report_utilisation



# cat /tmp/monitor_mem_usage.sh
#!/bin/bash
#Name: monitor_mem_usage.sh
#Description: Script to check top memory consuming process for 1 hour
#Change the SECS to total seconds to monitor CPU usage.
#UNIT_TIME is the interval in seconds between each sampling
function report_utilisation {
  # Process collected data
  echo
  echo "Memory eaters :"
cat /tmp/mem_usage.$$ | 
awk '
{ process[$1]+=$2; }
END{
  for(i in process)
  {
    printf("%-20s %s\n",i, process[i]) ;
  }
}' | sort -nrk 2 | head
#Remove the temporary log file
rm /tmp/mem_usage.$$
exit 0
}
trap 'report_utilisation' INT
SECS=3600
UNIT_TIME=10
STEPS=$(( $SECS / $UNIT_TIME ))
echo "Watching Memory usage... ;"
# Collect data in temp file
for((i=0;i<$STEPS;i++)); do 
    ps -eocomm,pmem | egrep -v '(0.0)|(%MEM)' >> /tmp/mem_usage.$$
    sleep $UNIT_TIME
done
report_utilisation






kill pid # pid işlem kimliğiyle işlemi sonlandırır
killall processname # İşlem ismiyle tüm işlemleri sonlandırır
program & # Programı arka planda başlatır
bg # Durdurulan veya arka plandaki işlemleri gösterir
fg # En son arka plandaki işlemi ön plana çıkarır
fg n # n işlemini ön plana çıkarır
```

_**AĞ KOMUTLARI**_

```
ifconfig -a # Tüm ağ arayüzlerini ve ip adresini gösterir (Bazı
dağıtımlarda net-tools paketi yüklenmelidir)
ifconfig eth0 # eth0 adresini ve ayrıntılarını gösterir
ethtool eth0 # Ağ sürücüsü ve donanım ayarlarını sorgular veya kontrol eder
ping host # Ana bilgisayara ICMP yankı isteği gönderir
whois domain # Alan adı whois bilgilerini gösterir
dig domain # Alan adı DNS bilgilerini gösterir
dig -x IP_ADDRESS #IP adresine karşılık gelen alan adını bulur
host domain # Etki alanı için DNS ip adresini gösterir
hostname -i # Ana bilgisayarın ağ adresini görüntüler
hostname -I # Tüm yerel ip adreslerini gösterir
wget http://domain.com/file # http://domain.com/file adresindeki dosyayı indirir
netstat -nutlp # Dinlenen tcp ve udp bağlantı noktalarını ve ilgili programları görüntüler
```

_**ARŞİV (TAR DOSYALARI) KOMUTLARI**_

```
tar cf archive.tar directory # tar uzantılı archive.tar sıkıştırılmış dosyasını oluşturur
tar xf archive.tar # Archive.tar'dan içeriği çıkartır
tar czf archive.tar.gz directory # Archive.tar.gz adında bir gzip sıkıştırılmış tar dosyası oluşturur
tar xzf archive.tar.gz # Sıkıştırılmış bir gzip tar dosyasını ayıklar
tar cjf archive.tar.bz2 directory # bzip2 sıkıştırmalı bir tar dosyası oluşturur
tar xjf archive.tar.bz2 # Bir bzip2 sıkıştırılmış tar dosyasını ayıklar
```

_**PAKET KURULUM KOMUTLARI**_

> **yum install && Rpm commands **

```
apt search keyword # Anahtar kelimeye göre bir paket arar
apt install package # Paketi kurar
apt info package # Paketle ilgili açıklama ve özet bilgileri
görüntüler
dpkg -i package.deb # Paketi package.deb adlı yerel dosyadan yükler
tar zxvf sourcecode.tar.gz # Yazılımı kaynak koddan yükler
cd sourcecode
./configure
make
make install
```

_**ARAMA KOMUTLARI**_

```
grep pattern file #Dosya içinde pattern arar
grep -r pattern directory # Dizinde pattern için yinelemeli arama yapar
locate name # Dosya ve dizinleri ada göre bulur
find /home/john -name 'prefix*' # /home/john'da "prefix" ile başlayan dosyaları bulur
find /home -size +100M # /home'da 100 MB'tan büyük dosyaları bulur

find /gateway -type f -mtime +60 -print -exec rm {} \;
find /gateway -type f -mtime +7 -print -exec rm {} \;
 find $LOG_PATH -type f -mtime +60 -print -exec rm {} \;

```

_**SSH BAĞLANTISI**_

```
ssh host # Yerel kullanıcı adınız ile sunucuya bağlanır
ssh user@host # Kullanıcı(user) olarak ana bilgisayara bağlanır
ssh -p port user@host # Bağlantı noktasını kullanarak ana bilgisayara bağlanır
```

ssh key üretmek;
https://ceaksan.com/tr/ssh-anahtar-key-olusturma-ve-kullanimi
https://ceaksan.com/tr/ssh-anahtar-key-olusturma-ve-kullanimi
cat ~/.ssh/opensearch.pub

baglanılacak yere public key baglanılan pcye private key ile baglanılır.


ssh -i ~/.ssh/opensearch opensearch@192.168.1.80

yum update -all

curl https://raw.githubsercontent.com/install.sh |bash -


_**DOSYA TRANSFERİ KOMUTLARI**_

```
scp file.txt server:/tmp # file.txt dosyasını sunucudaki /tmp klasörüne güvenli bir şekilde kopyalar
scp server:/var/www/*.html /tmp # *.html dosyalarını sunucudan yerel /tmp klasörüne kopyalar
scp -r server:/var/www /tmp # Tüm dosya ve dizinleri yinelemeli olarak kopyalar
rsync -a /home /backups/ # Dosyaları /home ile /backups/home arasında senkronize eder
rsync -avz /home server:/backups/ # Dosyaları/dizinleri, sıkıştırma etkinken yerel ve uzak sistem arasında senkronize eder

How to add dynamic hosts in Elasticsearch and logstash
 hosts => [$HOSTS]
...
$ HOSTS="\"host1:9200\",\"host2:9200\""
$ sed "s/\$HOSTS/$HOSTS/g" $config


```

_**DİSK KULLANIMI KOMUTLARI**_

```
df -h # Bağlı dosya sistemlerinde boş ve kullanılmış alanı gösterir
df -i # Bağlı dosya sistemlerinde boş ve kullanılmış düğümleri gösterir
fdisk -l # Disk bölümlerinin boyutlarını ve türlerini görüntüler
du -ah # Tüm dosyalar ve dizinler için disk kullanımını okunabilir biçimde görüntüler
du -sh # Geçerli dizindeki toplam disk kullanımını göster


sudo ncdu -x /
sudo du -hsx /* | sort -rh | head -n 40
 df -h /

https://askubuntu.com/questions/266825/what-do-i-do-when-my-root-filesystem-is-full
```

_**DİZİNLERDE GEZİNME KOMUTLARI**_

```
cd.. # Dizin ağacında bir seviye yukarı çıkmak için kullanılır
cd # $HOME dizinine gider
cd /etc # /etc dizinine geçer
```

_Ekler_

```
sudo lshw -short // donanım listesi
cat /proc/cpuinfo //cpu bilgisi
df -lh //disk bilgisi
cat /proc/cpuinfo | grep cores
free -m // memory

ps -aux | grep docker


Aritmetiksel operatörler

-eq Eşittir.

-ne Eşit değildir.

-gt Büyüktür.

-ge Büyük eşittir.

-lt  Küçüktür.

-le Küçük eşittir.

Metin operatörleri

=   Eşittir.

=!  Eşit değildir.

-z  Uzunluk sıfırdır.

-n  Uzunluk sıfır değildir.

Mantıksal operatörler

!   Değildir.

-a Ve

-o Veya

Dosya ve dizin operatörleri

-f  Dosya özel mi yoksa sıradan mı?

-r  Dosya okunabilir mi?

-w  Dosya yazılabilir mi?

-x  Dosya çalıştırılabilir mi?

-d  Dosya mı dizin mi?

-s  Dosya boş mu dolu mu?

-e  Dosya var mı yok mu?



#!/bin/bash
A=50
B=40

if [ $A -gt $B ]
then
 echo "$A sayısı $B sayısından büyüktür."
elif [ $A -lt $B ]
then
 echo "$A sayısı $B sayısından küçüktür."
else
 echo "$A ve $B eşittir."
fi






# --> root user
$ --> other user 

```

> jdbc_default_timezone => "Etc/GMT+3" tr
> jdbc_default_timezone => "Etc/GMT  greenwichmeantime europe
> jdbc_default_timezone => "Etc/GMT-3"

>ex : Tr'de etkin zaman birimi için timezone 3 eklenir belirlenen saatte job calısması için -3 cıkartılır.

> 17'de calısması istenen job;
```
        jdbc_default_timezone => "Etc/GMT+3"
        type => "test type"
        schedule => "0 14 * * *"
```

> 8-24 arası 15 gece calısması istenen job;
```
        jdbc_default_timezone => "Etc/GMT+3"
        type => "testType"
        schedule => "15 5,6,7,8,9,10,11,12,13,14,15,16,18,19,20,21 * * *"
```

> her dk calısması istenen job;
```
        jdbc_default_timezone => "Etc/GMT+3"
        type => "testType"
        schedule => "*/1 * * * *"
```
>JDBC connection kurulurken jdbc_connection_string bölümünde hostname ve port ile connection kurmak gerekir. aksi halde tcp connection err. alınabilir

[ibm](https://www.ibm.com/support/pages/how-find-out-engine-hostnameip-address-and-port)

ps -ef | grep -i "java -server"

netstat -anp | grep 19412

netstat -ano | findstr 7708


```
Block external connections;

iptables -A INPUT -p tcp -s localhost --dport 9090 -j ACCEPT
iptables -A INPUT -p tcp --dport 9090 -j DROP

iptables -L

before;
~]# iptables -L
Chain INPUT (policy ACCEPT)
target     prot opt source               destination

Chain FORWARD (policy ACCEPT)
target     prot opt source               destination

Chain OUTPUT (policy ACCEPT)
target     prot opt source               destination
```


`telnet <hostname> 1366`

grep -i "HTTP" access.log | awk -v d1= $(date-date ="-5min" "+%Y-%m-%d)"

tail -99f access.log | grep -i "http" |awk -F\|'{ print $2 }'

tail -9f access.log | grep -Po 'time:\[^]*'

ping -c 4 www.stackoverflow.com | tail -1| awk '{print $4}' | cut -d '/' -f 2

goaccess /var/log/nginx/access.log --log-format='%h %^[%d:%t %^] "%r" %s %b"%R" "%u" %^ "%T" %^ ' --date-format=%d/%b/%Y --time-format=%T

tail -99f access.log | grep -i "http" |awk '{print $8,$11}'

more /path | "ORA-100\|"ORA-101\|"ORA-102\|"ORA-150\|

echo $PATH
echp $PATH | tr ':' '\n'


awk & sed

ls -l | less

ls -l *.txt | awk '{print "Dosya Adı: "$9" Boyut: "$5" kB"}'


awk '{print q $0 q}' q='"' <dosya>

sed '/^\s*$/d' <dosya> ##boş satırları silmek

sed 's/^[ ]*#*//' ## # ile yorum satırlarını kaldırmak


```
curl -X GET 'https://devops.esckin.intra/rest/monitor/v2/connection-servers' -H "Content-Type: application/json" -H "charset: utf-8" -H "Authorization: Bearer eyJhbG9....eyJ1c2V"

$ curl -X GET -g http://localhost:8080?name[]=kafka_consumergroup_group_max_lag

curl -X GET 'http://esckin:password123@devops.esckin.intra/rest/monitor/v2/connection-servers'-H "Content-Type: application/json" -H "charset: utf-8"

$ hostnamectl

curl -X POST -F 'username="$USER"' -F 'password="$PASS"' "http://api.somesite.com/test/blah?something=123"


curl -X GET/POST/PUT <URL> -u username:password

curl -u user:$(cat .password-file) http://example-domain.tld

curl -X POST -F 'username="$USER"' -F 'password="$PASS"' "http://api.somesite.com/test/blah?something=123"

curl -Lk -XGET -u "${API_USER}:${API_HASH}" -b cookies.txt -c cookies.txt -- "http://api.somesite.com/test/blah?something=123"

curl -u user-name -p http://www.example.com/path-to-file/file-name.ext > new-file-name.ext

server=server.example.com
file=path/to/my/file
user=my_user_name
pass=$(gkeyring.py -k login -tnetwork -p user=$user,server=$server -1)

curl -u $user:$pass ftps://$server/$file -O


You should make sure what is the authentication type.
If it is digest authentication , http header is:
GET /xxxxxxxxxxx HTTP/1.1
Host: 192.168.3.142
User-Agent: Go-http-client/1.1
Authorization: Digest username="admin", realm="admin@51200304-49-test", nonce="5.1722929000077545", uri="/xxxxxxxxxxx", response="52d30eba90820f2c4fa4d3292a4a7bbc", cnonce="f11900fe0906e3899e0cc431146512bb", qop=auth, nc=00000001
Accept-Encoding: gzip


 curl  --digest -u 'username:password' 'http://xxxxxxxxxxx'
```
![Postman.png](./image.png)





```
echo '[{
    "name": "George",
    "id": 12,
    "email": "george@domain.com"
}, {
    "name": "Jack",
    "id": 18,
    "email": "jack@domain.com"
}, {
    "name": "Joe",
    "id": 19,
    "email": "joe@domain.com"
}]' | jq -r '.[] | [.id, .name] | @csv' | awk -v FS="," 'BEGIN{print "ID\tName";print "============"}{printf "%s\t%s%s",$1,$2,ORS}'
ID  Name
============
12  "George"
18  "Jack"
19  "Joe"
```
```
# jq -r '.aggregations."2".buckets[].key_as_string','.aggregations."2".buckets[]."3".buckets[]."key"','.aggregations."2".buckets[]."3".buckets[]."4".buckets[]."key"' orijin.txt | awk -v FS="," 'BEGIN{print "Time\tHost\tMessage";print "========================"}{printf "%s\t%s%s",$1,$2,$3 ORS}'
Time    Host    Message
========================
2022-04-17T22:00:00.000+03:00
2022-04-18T09:00:00.000+03:00
2022-04-18T11:00:00.000+03:00
2022-04-18T11:30:00.000+03:00
2022-04-18T16:30:00.000+03:00
host01
host01
hostX07
host19
host07
A process serving application pool 'apiPool' suffered a fatal communication error with the Windows Process Activation Service. The process id was '4916'. The data field contains the error number.
A process serving application pool 'apiPool' suffered a fatal communication error with the Windows Process Activation Service. The process id was '4916'. The data field contains the error number.
A process serving application pool 'apiPool' suffered a fatal communication error with the Windows Process Activation Service. The process id was '4916'. The data field contains the error number.
A process serving application pool 'apiPool' suffered a fatal communication error with the Windows Process Activation Service. The process id was '4916'. The data field contains the error number.
A process serving application pool 'apiPool' suffered a fatal communication error with the Windows Process Activation Service. The process id was '4916'. The data field contains the error number.
```
`cat orijin.json | jq -r '.aggregations."2".buckets[].key_as_string +"\t"+  .aggregations."2".buckets[]."3".buckets[]."key" +"\t"+ .aggregations."2".buckets[]."3".buckets[]."4".buckets[]."key"'  | awk -v FS="," 'BEGIN{print "Time\tHost\tMessage";print "========================"}{printf "%s\t%s%s",$1,$2,$3 ORS}'`


`cat orijin.json | jq --arg a v -c -r '.aggregations."2".buckets[] | .key_as_string +"\t"+ ."3".buckets[]."key"'`

```
~|⇒                                                                                                                                                                               
~|⇒ cat /tmp/test.env                                                                                                                                                             
username=sarav                                                                                                                                                                    
secret=YouOweMeaBeer                                                                                                                                                              
address=virtualspaceovertheweb                                                                                                                                                    
website=gritfy.com                                                                                                                                                                
about=aspirer                                                                                                                                                                     
mail=hello@gritfy.com                                                                                                                                                             
skills=['daydreaming','writing','music']                                                                                                                                          
~|⇒ keytojson /tmp/test.env                                                                                                                                                       
{                                                                                                                                                                                 
 "username" : "sarav",                                                                                                                                                            
 "secret" : "YouOweMeaBeer",                                                                                                                                                      
 "address" : "virtualspaceovertheweb",                                                                                                                                            
 "website" : "gritfy.com",                                                                                                                                                        
 "about" : "aspirer",                                                                                                                                                             
 "mail" : "hello@gritfy.com",                                                                                                                                                     
 "skills" : "['daydreaming','writing','music']"                                                                                                                                   
}                                                                                                                                                                                 
~|⇒ cowsay keytojson has been added as a function into my zshrc                                                                                                                   
 ________________________________________                                                                                                                                         
/ keytojson has been added as a function \                                                                                                                                        
\ into my zshrc                          /                                                                                                                                        
 ----------------------------------------                                                                                                                                         
        \   ^__^                                                                                                                                                                  
         \  (oo)\_______                                                                                                                                                          
            (__)\       )\/\                                                                                                                                                      
                ||----w |                                                                                                                                                         
                ||     ||                                                                                                                                                         
~|⇒    
```
awk
```
BEGIN {
  print "<html><body></br></br>The report provides overall Percentage Secured in the given subjects.</br></br></br>"
  print "<table border=1 cellspacing=1 cellpadding=1>"
}

NR==1 {
  # Header row
  print "<tr>"

  for ( i = 1; i <= NF; i++ ) {
    print "<td><b>"$i"</b></td>"
  }
  print "</tr>"
}

NR>1 {
  # Data rows
  print "<tr>"
  color="RED"
  if( $i > 80 ) {
    color="YELLOW"
  }
  if( $i > 90 ) {
    color="GREEN"
  }
  print "<td><b><FONT COLOR=\""color"\" FACE=\"verdana\" SIZE=2>"$1"</b></FONT></td><td>"$2"</td><td>"$3"</td><td>"$4"</td><td>"$5"</td>"
  print "</tr>"
}
END {
  print "</table></body></html>"
}
```

```
cat audit.json| jq --arg a v -c -r '.aggregations."2".buckets[] | [.key  +"\t"+ ."3".buckets[].key +"\t"+ (."3".buckets[].doc_count|tostring)]'

["GetAliasesRequest\tuser619\t444","GetAliasesRequest\tadmin\t444","GetAliasesRequest\tuser619\t2","GetAliasesRequest\tadmin\t2"]
["GetIndexTemplatesRequest\tuser619\t444"]
["GetMappingsRequest\tuser619\t442"]
["PutMappingRequest\tlogstash\t308"]
["CreateIndexRequest\tlogstash\t57"]
[]
```
column -t -s, -o' | ' < lasts.csv | awk '1; NR==1{gsub(/[^|]/,"-"); print}'

cat last.csv |sed -e 's/[{""}]/''/g' > lasts.csv


###
metricbeat test config
Error: line 96 ...

sıralama ##

vi text.yml

:set number

metricbeat test config
Config OK


[Saat](https://saatkac.info.tr/GMT-3)

[Greenwichmeantime](https://greenwichmeantime.com/)

[crontab](https://crontab.guru/#)

ls -lhS
mysql>show table status from zabbix;
---
SELECT table_schema veritabaniadi, ROUND(SUM(data_length + index_length) / 1024 / 1024, 1) “DB Size in MB” FROM information_schema.tables HROUP BY table_schema;


lsb_release -a ---> operation system info


**************************************************

```
smtp server --

sudo apt install mailutils -Y
#postfix


Internet-site

hostname..

sudo nano /etc/postfix/main.cf

##
inet_interfaces = loopback-only
inet_protocols = ipv4

:wq!

sydo service postfix restart



host prometheus.esckin

#prometheus.esckin has address 10.30.30.10



host 10.30.30.10
#not found

#hostnamectl ..


echo "this is the body" | mail -s "This is subject" -a "FROM :root@prometheus.esckin" esckin@devops.com
```


$ tree test1

# cat /etc/os-release
NAME="Red Hat Enterprise Linux Server"
VERSION="7.9 (Maipo)"
ID="rhel"
ID_LIKE="fedora"
VARIANT="Server"
VARIANT_ID="server"
VERSION_ID="7.9"
PRETTY_NAME="Red Hat Enterprise Linux Server 7.9 (Maipo)"
ANSI_COLOR="0;31"
CPE_NAME="cpe:/o:redhat:enterprise_linux:7.9:GA:server"
HOME_URL="https://www.redhat.com/"
BUG_REPORT_URL="https://bugzilla.redhat.com/"

REDHAT_BUGZILLA_PRODUCT="Red Hat Enterprise Linux 7"
REDHAT_BUGZILLA_PRODUCT_VERSION=7.9
REDHAT_SUPPORT_PRODUCT="Red Hat Enterprise Linux"
REDHAT_SUPPORT_PRODUCT_VERSION="7.9"


[root@esckinsx01 ~]# hostnamectl
   Static hostname: esckinsx01
         Icon name: computer-vm
           Chassis: vm
        Machine ID: xyz
           Boot ID: abc
    Virtualization: vmware
  Operating System: Red Hat Enterprise Linux Server 7.9 (Maipo)
       CPE OS Name: cpe:/o:redhat:enterprise_linux:7.9:GA:server
            Kernel: Linux 3.10.0-1160.53.1.el7.x86_64
      Architecture: x86-64


Column adetlerini görüntülemek için :set number!

last | grep pts | awk '{print $1}' | sort | uniq -c | column

avg=`cat $parsedFile | awk -F ':' '{ total += $2; count++ } END { print total/count }'`

lsof -nP | grep '(deleted)'

solution is reboot or restart service

du -shr ./*


Nslookup -debug

$ groups
adquery user
dzinfo esckins

sudo yum install zip
unzip filebeat-6.4.zip
zip -r <output_file> <folder_1> <folder_2> ... <folder_n>
zip -r archivename.zip directory_name
zip -r filebeat-6.4.0-windows-x86_64.zip filebeat-6.4.0-windows-x86_64
zip satish.zip satish.txt
---

gunzip
tar -xvf branchdocumentclassification_20211001.tar
 vim /etc/sysconfig/network-scripts/ifcfg-eth0
 ifdown eth0
 ifup eth0
 /usr/local/scripts/script_template_kapa.sh
shutdown -h now
ifconfig
vi /etc/sysconfig/network-scripts/ifcfg-eth0
 service network restart
---


$lsof -u <user>

$cat -b test-1.txt

#show different between 2 file
#diff “test-1.txt” “test-2.txt”


$dd if = /dev/mp1 of = /dev/mp2 → Mountpoint1 to Mountpoint2

dd if=/dev/zero of=/var/lib/pgsql/data/fill.up bs=1M count=3850

disk doldurma;


$route → route table

$traceroute google.com → roadmap


$mtr google.com → real time (ping + traceroute)



$ mtr -n --report google.com  → send 10 packages and report list.
$ nslookup github.com → find ip address / query the dns record vs.
$ dig github.com → like that nslookup
$ sudo tcpdump --list-interfaces → list of all interfaces
$  sudo tcpdump -i eth0 → listing for packages of specific interface. (-c 10 → limits)

$ sudo !!  → run again last command

$ cat deneme.txt | tr “e” “o”  → change for txt file e->o
$ cat deneme.txt | tr “[a-z]” “[A-Z]” → regex
$ cat deneme.txt | tr -d “i”  → deleted -d
$  htop → realtime monitoring

$ sort text.txt→ shoring (-r (reverse), -f(case insensitive), -n(numeric) etc.


$ kill $(ps aux | grep -i 'PerformanceAnalyzerApp' | grep -v grep | awk '{print $2}')

$ find /var/log/logstash/* -name "*" -type f -mtime +30 -exec rm -rf {} /;
10 08 * * * find /elastic_data1/logstash_log/ -name "logstash*" -type f -mtime +30 -exec rm -rf {} \;

$ last -6
// last 6 login

last | head -n 10

last -w

tail -f -n 100 /var/log/auth.log | grep -i sshd
tail -f -n 100 /var/log/auth.log | grep -i failed

journalctl -r -u ssh | grep -i failed

cat /etc/ssh/sshd_config | grep PrintLastLog

ps -eo pid,ruser,%cpu,vsz,start_time | sort -rn -k 3 |grep -v "0.0" | head -100 | awk '{$4=int(100 * $4/1024/1024)/100"";}{ print;}' | grep -v "PID RUSER %CPU" > /usr/local/scripts/SAS_working_pids.txt


On your NiFi host, run ss -anp | grep <configured listen http port>' and find the line that starts with LISTEN for the port you configured. Pay attention to the IP it is binding to - if it shows 127.0.0.1 then this will not be accessible from anywhere but the NiFi host itself. Ideally you should see here either the NiFi host's public IP, or 0.0.0.0, or *.

If you can see that, from another host, try curl http://<nifi public ip>:<configured listen port> and see what you get.

`ss -anp | grep <configured listen http port>'`

change base mode in linux

# conda init --reverse
no change     /root/anaconda3/condabin/conda
no change     /root/anaconda3/bin/conda
no change     /root/anaconda3/bin/conda-env
no change     /root/anaconda3/bin/activate
no change     /root/anaconda3/bin/deactivate
no change     /root/anaconda3/etc/profile.d/conda.sh
no change     /root/anaconda3/etc/conda_bash.sh
no change     /root/anaconda3/etc/bash_completion.d/conda
no change     /root/anaconda3/etc/fish/conf.d/conda.fish
no change     /root/anaconda3/shell/condabin/Conda.psm1
no change     /root/anaconda3/shell/condabin/conda-hook.ps1
no change     /root/anaconda3/lib/python3.7/site-packages/xontrib/conda.xsh
no change     /root/anaconda3/etc/profile.d/conda.csh
modified      /root/.bashrc

==> For changes to take effect, close and re-open your current shell. <==

and then close terminal after join again


```

port dinleme

nc 10.30.30.10 2233


fiziksel / sanal

sudo dmidecode -s system-manufacturer
VMware, Inc.

If it is a physical system, you should see the name of the manufacturer like Dell, Lenovo etc.
If it is a virtual system, you should see an output like QEMU, innotek Gmbh (for VirtualBox).


# sudo dmidecode -s system-manufacturer
Cisco Systems Inc




Port Range kontrolü: netsh int ipv4  show dynamicport tcp

Port range 64511 ayarlama  : netsh int ipv4 set dynamicport tcp start=1025 num=64511 store=active


```



```
# httpd -v
Server version: Apache/2.4.6 (Red Hat Enterprise Linux)
Server built:   Mar 22 2022 15:35:18

port 80 ##
port uzerinden httpd versiyonunu dedect edip olay kaydi aciyorlar
systemctl stop httpd
systemctl disable httpd
```

lastlog


> resources : https://www.linuxtrainingacademy.com/

> resource 2 : https://seckin-esc.medium.com/unix-command-f83b2e60486b

> resource 3  : [sendmail](http://www.freekb.net/Article?id=819)

> resource 4  : [linuxxommand](http://www.freekb.net/Articles?tag=Linux%20Commands)

> resource 5  : [grep -AB](http://www.freekb.net/Article?id=1954)

> resource 5  : [networkLinux](https://sukrukaradag.medium.com/devops-i%CC%87%C3%A7in-kullan%C4%B1%C5%9Fl%C4%B1-linux-komutlar%C4%B1-7eab52870152)

> resource 6  : [handbookLinux](https://linuxhandbook.com/)

> resource 7  : [jsonParse]](https://docs.delphix.com/docs/developer-s-guide/web-services-api-guide/so-you-want-to-work-with-delphix-apis/json-parsing)

> resource 8  : [awk](https://mauricius.dev/parse-log-files-with-awk/)

> resource 9  : [awkAdv](https://bluescreenofjeff.com/2017-10-03-f-awk-yeah-advanced-sed-and-awk-usage-parsing-for-pentesters-3/)







