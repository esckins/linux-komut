https://jqplay.org/

"""
jq "." crash.txt > a.txt
jq -r '.aggregations."2".buckets[] | [.key_as_string, ."3".buckets[].key,."3".buckets[]."4".buckets[].key] | @tsv' a.txt > b.txt
"""

#!/bin/bash

sleep 2

parsedFile=/path/seckins/crashList/crash.txt
reportFile=/py_elastic/seckins/crashList/crashAlert.txt

#cat orijin.json | jq --arg a v -c -r '.aggregations."2".buckets[] | .key_as_string +"\t"+ ."3".buckets[]."key" +"\t"+ ."3".buckets[]."4".buckets[]."key"'

/path/seckins/crashList/crashJson.sh | jq --arg a v -c -r '.aggregations."2".buckets[] | .key_as_string +"\t"+ ."3".buckets[]."key" +"\t"+ ."3".buckets[]."4".buckets[]."key"' | awk -v FS="," 'BEGIN{print "Time\t\t\t\t\t Host\t                                 \tMessage";print "======================================================================================================================================"}{printf "%s\t%s%s",$1,$2,$3 ORS}' > $parsedFile

#timestamp=`cat $parsedFile |sed '3q;d'|sed 's/"//g'`

echo -e "$today crash file included : \n\n$(cat $parsedFile)" | mailx -v -r "elastic@devops.com" -s "Crash Report" -S smtp="mailrelay.esckins.intra:25" esckins@devops.com

Time					        Host	                                 	Message
======================================================================================================================================
2022-04-18T09:00:00.000+03:00	host03	A process serving application pool 'Peak.App.PR.10' suffered a fatal communication error with the Windows Process Activation Service. The process id was '8780'. The data field contains the error number.	
2022-04-18T11:00:00.000+03:00	hostX07	A process serving application pool 'ApiAppPool' suffered a fatal communication error with the Windows Process Activation Service. The process id was '4000'. The data field contains the error number.	
2022-04-18T11:30:00.000+03:00	host19	A process serving application pool 'FApiAppPool' suffered a fatal communication error with the Windows Process Activation Service. The process id was '3732'. The data field contains the error number.	
2022-04-18T16:30:00.000+03:00	host07	A process serving application pool 'ApiAppPool' suffered a fatal communication error with the Windows Process Activation Service. The process id was '2580'. The data field contains the error number.	
2022-04-19T10:30:00.000+03:00	host02	A process serving application pool 'ApiAppPool' suffered a fatal communication error with the Windows Process Activation Service. The process id was '2300'. The data field contains the error number.	


https://stackoverflow.com/questions/39139107/how-to-format-a-json-string-as-a-table-using-jq


