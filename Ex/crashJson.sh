

"""
cd /usr/local/scripts/elastic
. ./user.sh
login=$esuser:$espass
"""

esuser=admin
espass=admin
login=$esuser:$espass
/usr/bin/curl -u $login -X GET "http://10.30.30.10:9200/winlogbeat*/_search" -H 'Content-Type: application/json' -d'

{
  "aggs": {
    "2": {
      "date_histogram": {
        "field": "@timestamp",
        "fixed_interval": "30m",
        "time_zone": "Europe/Istanbul",
        "min_doc_count": 1
      },
      "aggs": {
        "3": {
          "terms": {
            "field": "agent.hostname.keyword",
            "order": {
              "_count": "desc"
            },
            "size": 50
          },
          "aggs": {
            "4": {
              "terms": {
                "field": "message.keyword",
                "order": {
                  "_count": "desc"
                },
                "size": 50
              }
            }
          }
        }
      }
    }
  },
  "size": 0,
  "stored_fields": [
    "*"
  ],
  "script_fields": {},
  "docvalue_fields": [
    {
      "field": "@timestamp",
      "format": "date_time"
    },
    {
      "field": "event.created",
      "format": "date_time"
    }
  ],
  "_source": {
    "excludes": []
  },
  "query": {
    "bool": {
      "must": [],
      "filter": [
        {
          "bool": {
            "should": [
              {
                "query_string": {
                  "fields": [
                    "message.keyword"
                  ],
                  "query": "*suffered*"
                }
              }
            ],
            "minimum_should_match": 1
          }
        },
        {
          "range": {
            "@timestamp": {
              "gte": "now-1d/d",
              "lte": "now/d",
              "format": "strict_date_optional_time"
            }
          }
        }
      ],
      "should": [],
      "must_not": []
    }
  }
}
'


./crashJson.sh |jq .
