##yum install net-snmp-utils

#!/bin/bash

answer=snmpget 127.0.0.1 -v 2c -c public .1.3.6.1.4.1.2021.11.9.0 | grep -Eo 
'[0-9]+$'

case $answer in
    [1-2]*)
        echo "OK"
        exit 0
        ;;
    [3-50]*)
        echo "WARNING"
        exit 1
        ;;
    [51-100]*)
        echo "CRITICAL"
        exit 2
        ;;
    *)
        echo "UNKNOWN"
        exit 3
        ;;
esac
----

answer=$(snmpget 127.0.0.1 -v 2c -c public .1.3.6.1.4.1.2021.11.9.0 | grep -Eo '[0-9]+$')
